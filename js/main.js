/* 
   1. Метод об'єкту - це функція, яка визначається в контексті об'єкта і виконує задані дії

   2. Властивостями об'єкта можуть бути дані за типом: число, рядок, булеві, масиви, об'єкти, функції, null, undefined

   3. Це означає, що коли створюється змінна та присвоюється об'єкт, сам об'єкт не копіюється у змінну, але замість цього створюється посилання на об'єкт.
 */

function createNewUser() {
   const newUser = {};
   
   newUser.firstName = prompt("Ваше ім'я?");
   newUser.lastName = prompt("Ваше прізвище?");

   newUser.getLogin = function() {
      let str = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
      return str;
   }
   return newUser;
   
}

const user = createNewUser();

console.log(user);

const login = user.getLogin();

console.log(login);























